#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <unistd.h>

#include <linux/zio-user.h>
#include <adc-lib.h>
#include <adc-lib-compat.h>


#define ZIO_CARD_NAME "adc-ziofake"
#define GEN_CARD_NAME "adc-genericfake"
#define N_CHAN 1
#define RETRIEVE_CONF_TEST 2

#define GETOPT_STRING "z:gh"

unsigned int dev_id = 0;
unsigned int nshots = 4;
unsigned int presamples = 8;
unsigned int postsamples = 8;


void print_buffer(struct fmcadc_buffer *buf);

void genericfake_board();

void ziofake_board(char chan);

void fk_help();

void fk_help()
{
	printf("\nfk-test [OPTION]\n\n");
	printf("  -z<num>	ZIO device\n\n");
	printf("    		  <num>:   ZIO channels supported and output given:\n");
	printf("    		      		0: zeroes\n");
	printf("      	 		        1: random numbers\n");
	printf("        		        2: growing sequence\n");
	printf("        		        i: interleaved channel: zeroes, random\n");
	printf("           		           and growing numbers are alternated\n\n");
	printf("  -g		generic (not-ZIO) device\n\n");
	printf("  -h		show this help\n\n");
}


int main(int argc, char *argv[])
{
	char opt;
	
	for (opt = 1; (opt = getopt(argc, argv, GETOPT_STRING)) != -1;) {
		switch (opt) {
			case 'z':
				ziofake_board(*optarg);
			break;
			
			case 'g':
				genericfake_board();
			break;
			
			case 'h': default:
				fk_help();
			break;			
		}
	}
	
	return 0;

}

void print_buffer(struct fmcadc_buffer *buf)
{
	struct fmcadc_timestamp *ts = calloc(1, sizeof(*ts));
	ts = fmcadc_tstamp_buffer(buf, ts);
	int i, c;
	uint8_t *data = buf->data;
	
	fprintf(stdout, "TIMESTAMP %lli:%lli:%lli\n", (long long int)ts->secs, 
						      (long long int)ts->ticks, 
						      (long long int)ts->bins);
	
	fprintf(stderr, "nsamples: %d\n", buf->nsamples);
	for (i=0; i < buf->nsamples; i++) {
		fprintf(stdout, "sample %5i:	", i - presamples);
		for (c=0; c < N_CHAN; c++) {
			fprintf(stdout, "%10i ", *(data++));
		}
		fprintf(stdout, "\n");
	}
	free(ts);
}

void ziofake_board(char chan)
{
	struct fmcadc_dev *dev;
	int err, enable = 1, disable = 0, i;
	unsigned int nbuffer = nshots, ptr;
	unsigned long total_samples = nshots*(presamples + postsamples);
	struct fmcadc_conf acq, trg;
	
	switch (chan) {
		case 'i':
		case '0':
		case '1':
		case '2':
			fmcadc_init();
			
			dev = fmcadc_open(ZIO_CARD_NAME, dev_id, total_samples, nbuffer, (unsigned long)chan);
		break;
		default:
			fprintf(stderr, "Channel %c: not supported\n", chan);
			return;
		break;
	}
	
	if (!dev) {
		fprintf(stderr, "fmcadc_open: %s\n", fmcadc_strerror(errno));
		return;
	}	
	
	char chan_path[18];
	
	
	switch (chan) {
		case 'i': 
			err = fmcadc_set_param(dev, "cset0/chani/enable", NULL, &enable);
			
			if (err == -1) 
				fprintf(stderr, "chani not enabled: %s\n", fmcadc_strerror(errno));
		
			for (i = 0; i < 3; i++) {
				snprintf(chan_path, sizeof(chan_path), "cset0/chan%d/enable", i);
				fmcadc_set_param(dev, chan_path, NULL, &disable);	
			}
		break;
		
		case '0': 
			fmcadc_set_param(dev, "cset0/chani/enable", NULL, &disable);
			err = fmcadc_set_param(dev, "cset0/chan0/enable", NULL, &enable);
		break;
		
		case '1':
			fmcadc_set_param(dev, "cset0/chani/enable", NULL, &disable);
			err = fmcadc_set_param(dev, "cset0/chan1/enable", NULL, &enable);
		break;
		
		case '2':	
			fmcadc_set_param(dev, "cset0/chani/enable", NULL, &disable);
			err = fmcadc_set_param(dev, "cset0/chan2/enable", NULL, &enable);
		break;
	}

	
	memset(&acq, 0, sizeof(acq));
	acq.type = FMCADC_CONF_TYPE_ACQ;
	fmcadc_set_conf(&acq, FMCADC_CONF_ACQ_N_SHOTS, nshots);
	fmcadc_set_conf(&acq, FMCADC_CONF_ACQ_PRE_SAMP, presamples);
	fmcadc_set_conf(&acq, FMCADC_CONF_ACQ_POST_SAMP, postsamples);
	
	err = fmcadc_get_conf(&acq, FMCADC_CONF_ACQ_PRE_SAMP, &ptr);
	
	if (err == -1)
		fprintf(stderr, "fmcadc_get_conf(acq): %s\n", fmcadc_strerror(errno));
	
	err = fmcadc_apply_config(dev, 0, &acq);
	
	if (err)
		fprintf(stderr, "apply_config(acq): %s\n", fmcadc_strerror(errno));
	
	memset(&trg, 0, sizeof(trg));
	trg.type = FMCADC_CONF_TYPE_TRG;
	fmcadc_set_conf(&trg, FMCADC_CONF_TRG_SOURCE, 1);
	
	err = fmcadc_get_conf(&trg, FMCADC_CONF_TRG_SOURCE, &ptr);
	
	if (err == -1)
		fprintf(stderr, "fmcadc_get_conf(trg): %s\n", fmcadc_strerror(errno));
	
	err = fmcadc_apply_config(dev, 0, &acq);
	
	if (err)
		fprintf(stderr, "apply_config(acq): %s\n", fmcadc_strerror(errno));
	
	/*ACQ START*/
	err = fmcadc_acq_start(dev, 0, NULL);
	if (err)
		fprintf(stderr, "fmcadc_acq_start: %s\n", fmcadc_strerror(errno));
		
	
	
	for (i = 0; i < nshots; i++) {
		/*REQUEST BUFFER*/
		struct fmcadc_buffer *buf = fmcadc_request_buffer(dev, presamples + postsamples, NULL, 0);
		
		if (!buf) 
			fprintf(stderr, "fmcadc_request_buffer: %s\n", fmcadc_strerror(errno));
		
		/*FILL BUFFER*/
		err = fmcadc_fill_buffer(dev, buf, 0, NULL);
		if (err) 
			fprintf(stderr, "fmcadc_fill_buffer: %s\n", fmcadc_strerror(errno));
		
		print_buffer(buf);
		/*RELEASE BUFFER*/
		err = fmcadc_release_buffer(dev, buf, NULL);
		if (err) 
			fprintf(stderr, "fmcadc_release_buffer: %s\n", fmcadc_strerror(errno));
	}
	
	
	
	/*ACQ STOP*/
	err = fmcadc_acq_stop(dev, 0);
	
	if (err)
		fprintf(stderr, "fmcadc_acq_stop: %s\n", fmcadc_strerror(errno));
	
	
	err = fmcadc_close(dev);
	
	if (err)
		fprintf(stderr, "%s: fmcadc_close failed\n", __FILE__);
}


void genericfake_board()
{
	fmcadc_init();
	
	struct fmcadc_dev *dev = fmcadc_open(GEN_CARD_NAME,0,0,0,0);
	struct fmcadc_conf acq;
	struct fmcadc_buffer *buf;
	int err = 0, i;
	
	if (dev == NULL)
		fprintf(stderr, "fmcadc_genfake_open: %s\n", strerror(errno));
	
	memset(&acq, 0, sizeof(acq));
	
	acq.type = FMCADC_CONF_TYPE_ACQ;
	fmcadc_set_conf(&acq, FMCADC_CONF_ACQ_N_SHOTS, nshots);
	fmcadc_set_conf(&acq, FMCADC_CONF_ACQ_PRE_SAMP, presamples);
	fmcadc_set_conf(&acq, FMCADC_CONF_ACQ_POST_SAMP, postsamples);
	
	err = fmcadc_apply_config(dev, 0, &acq);
	
	if (err) 
		fprintf(stderr, "[line %d]: apply_config failed: %s\n", __LINE__, fmcadc_strerror(errno));
	
	int nsh;
	char *conf_path;
	asprintf(&conf_path, "%d/%d", FMCADC_CONF_TYPE_ACQ, FMCADC_CONF_ACQ_N_SHOTS);
	
	fmcadc_set_conf(&acq, FMCADC_CONF_ACQ_N_SHOTS, RETRIEVE_CONF_TEST);
	
	err = fmcadc_apply_config(dev, 0, &acq);
	if (err) 
		fprintf(stderr, "[line %d]: apply_config failed: %s\n", __LINE__, 
								fmcadc_strerror(errno));
	
	err = fmcadc_retrieve_config(dev, &acq);
	if (err) 
		fprintf(stderr, "[line %d]: retrieve_config failed: %s\n", __LINE__, 
								fmcadc_strerror(errno));
	fmcadc_get_param(dev, conf_path, NULL, &nsh);
	if (nsh != RETRIEVE_CONF_TEST)
		fprintf(stderr, "[%s] (line %d) retrieve_conf_test failed: nshots: %d\n", __FILE__, __LINE__, nsh);
	
	err = fmcadc_acq_start(dev, 0, NULL);
	if (err)
		fprintf(stderr, "[%s]: fmcadc_acq_start failed\n", __FILE__);	

	/*BUFFER*/
	
	for (i = 0; i < nshots; i++) {
		buf = fmcadc_request_buffer(dev, (presamples + postsamples), NULL, 0);
		if (buf == NULL)
			fprintf(stderr, "[line %d] request_buffer(%d) doesn't work!: %s\n", __LINE__, i, fmcadc_strerror(errno));
		
		err = fmcadc_fill_buffer(dev, buf, 0, NULL);
		if (err)
			fprintf(stderr, "Buffer(%d) not filled: %s\n", i, fmcadc_strerror(errno));
		
		print_buffer(buf);
		
		err = fmcadc_release_buffer(dev, buf, NULL);
		if (err)
			fprintf(stderr, "Buffer(%d) not released\n", i);
	}
	
	
	err = fmcadc_acq_stop(dev, 0);
	if (err != 0)
		fprintf(stderr, "[%s]: fmcadc_acq_stop failed!\n", __FILE__);
	
	fmcadc_close(dev);
	
}



